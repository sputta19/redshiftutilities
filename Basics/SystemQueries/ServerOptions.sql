-- show all server options available
	
	show all;

-- show options specific
	
	show datestyle;
	
	show describe_field_name_in_uppercase;
	
	show enable_result_cache_for_session;


-- #Query Group

	show query_group;
	
	-- set query group to label queries in session. 
	
	set query_group to 'sharath';
	
	select * from users limit 10;
	
	select query, pid, substring, elapsed, label from svl_qlog where label = 'sharath';
	
-- #wlm_query_slot_count : work load management
	-- number of query slots a query will use
	-- fixed amount of resources that can be assigned to user query. 

	set wlm_query_slot_count to 3;
	
-- #search_path : order in which schemas are searched. 
    
	create schema enterprise;
    
    set search_path to enterpirse;
    
    show search_path;
    

-- #Other options that can be set. 

	set analyze_threshold_percent to 10;
	
	set datestyle to 'SQL, MDY';
	
	set statement_timeout to 1;
	
	set timezone = 'America/Chicago'; 





